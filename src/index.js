import Vue from 'vue'
import App from './App'

const vueApp = new Vue({
  el: '#app',
  components: {
    App
  },
  render: el => el('App')
})