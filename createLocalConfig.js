// Create local server config file (if it doesn't exists)
const fs = require('fs')

console.log('Running post-install script...')

if ( !fs.existsSync('app/config/local.json') ) {
  // Not found, creating one with default settings
  console.log('Creating defaul local server settings')

  let defaultSettings = {
    server: {
      port: 27015
    }
  }

  fs.writeFileSync('app/config/local.json', JSON.stringify(defaultSettings))

  console.log('Created default settings file')
}
else {
  // File found, skip
  console.log('Default settings found, skipping...')
}