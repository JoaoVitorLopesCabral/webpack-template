const path = require('path')
const WebpackBuildNotifierPlugin = require('webpack-build-notifier')

module.exports = {
  entry: './app/backend.ts',
  output: {
    path: path.resolve(__dirname, "app"),
    filename: 'backend.js'
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'ts-loader'
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.json', '.ts']
  },
  plugins: [
    new WebpackBuildNotifierPlugin({
      title: 'Webpack - Backend build'
    })
  ],
  externals: {
    express: 'commonjs express'
  }
}