# Raewor Webpack Template

This template focus on running different build configurations for backend and frontend.

## Requirements

Before building, run `npm install`. This will install all required packages and create default server settings.

## Running all builds and watchers in the same terminal

To do so, please install **concurrently**

```bash
npm install -g concurrently
```

Then run `npm start`. This will run all required scripts to build/watch/serve the application.

## Without concurrently

To build and server without concurrently installed, you have to execute the following commands in separated terminal windows:

```bash
npm run watch-frontend
```

```bash
npm run watch-backend
```

```bash
npm run run-server
```