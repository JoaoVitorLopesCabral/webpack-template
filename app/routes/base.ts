import * as express from 'express'

const baseRoutes = express.Router()

baseRoutes.get('/', (req, res) => {
  res.render('index', {
    message: 'Hello there!'
  })
})

export {
  baseRoutes
}