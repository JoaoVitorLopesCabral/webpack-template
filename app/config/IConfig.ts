/**
 * Network config interface
 */
interface ServerNetworkConfigInterface {
  port: number
}
/**
 * Server config interface
 */
export interface IConfig {
  server: ServerNetworkConfigInterface
}