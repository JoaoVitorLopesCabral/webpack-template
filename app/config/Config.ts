import * as prodConfig from './prod.json'
import * as localConfig from './local.json'

import { IConfig } from './IConfig';

/**
 * Server configuration class
 */
export class Config {
  /**
   * Return a object with server configuration
   */
  public static getConfig() : IConfig {
    // Check current enviroment and return
    // corresponding config
    if ( process.env.NODE_ENV === 'production' ) {
      return prodConfig
    }    

    return localConfig
  }
}