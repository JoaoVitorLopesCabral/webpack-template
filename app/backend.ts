import * as express from 'express'
import { Config } from './config/Config'
import { baseRoutes } from './routes/base';

const app = express()
const serverConfig = Config.getConfig()

// Set public directory
app.use(express.static('public'))

// Set view directory
app.set('views', 'app/views')

// Setup pug
app.set('view engine', 'pug')

app.use(baseRoutes)

app.listen(serverConfig.server.port, () => {
  console.log(`Server listening on port ${serverConfig.server.port}`)
})